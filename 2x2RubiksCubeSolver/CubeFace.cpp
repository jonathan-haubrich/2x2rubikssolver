﻿#include "CubeFace.hpp"

std::ostream& operator<<(std::ostream& os, const CubeFace& cf)
{
	for (int i = 0; i < _countof(cf._squares); ++i)
	{
		for (int j = 0; j < _countof(cf._squares[i]); ++j)
		{
			os << cf._squares[i][j] << " ";
		}
	}

	return os;
}

bool CubeFace::operator==(const CubeFace& other) const
{
	return (_squares[0][0] == other._squares[0][0]) &&
		(_squares[0][1] == other._squares[0][1]) &&
		(_squares[1][0] == other._squares[1][0]) &&
		(_squares[1][1] == other._squares[1][1]);
}

CubeFace& CubeFace::operator=(const CubeFace& rhs)
{
	_squares[0][0] = rhs._squares[0][0];
	_squares[0][1] = rhs._squares[0][1];
	_squares[1][0] = rhs._squares[1][0];
	_squares[1][1] = rhs._squares[1][1];

	return *this;
}

CubeFace::CubeFace() : CubeFace('Z') {}

CubeFace::CubeFace(char id)
{
	_squares[0][0] = _squares[0][1] = _squares[1][0] = _squares[1][1] = id;
}

CubeFace::CubeFace(const std::string& face)
{
	_squares[0][0] = _squares[0][1] = _squares[1][0] = _squares[1][1] = ' ';
	int face_idx = 0;

	if (face.length() != ROWS * COLUMNS)
	{
		throw invalid_argument("Input for face must be equal to ROWS * COLUMNS");
	}

	for (int i = 0; i < ROWS; ++i)
	{
		for (int j = 0; j < COLUMNS; ++j)
		{
			face_idx = i * ROWS;
			face_idx +=  j;
			_squares[i][j] = face.at(face_idx);
		}
	}
}

char* CubeFace::GetRow(int row) const
{
	if (row >= ROWS)
	{
		throw invalid_argument("CubeFace::GetRow: row out of range");
	}

	// Dangerous cast. Annoying const requirements. Refactor probably
	return (char *)_squares[row];
}

const char* CubeFace::GetSquares() const
{
	return reinterpret_cast<const char*>(_squares);
}

char CubeFace::GetPosition(int i, int j) const
{
	if (i >= ROWS || j >= COLUMNS)
	{
		throw invalid_argument("CubeFace::GetPosition: row or column out of range");
	}

	return _squares[i][j];
}

int CubeFace::AsInt() const
{
	return (_squares[1][1] << 24) | (_squares[1][0] << 16) | (_squares[0][1] << 8) | (_squares[0][0]);
}

void CubeFace::Rotate(int direction)
{
	char tmp = _squares[0][0];

	if (LEFT == direction)
	{
		_squares[0][0] = _squares[0][1];
		_squares[0][1] = _squares[1][1];
		_squares[1][1] = _squares[1][0];
		_squares[1][0] = tmp;
	}
	else
	{
		_squares[0][0] = _squares[1][0];
		_squares[1][0] = _squares[1][1];
		_squares[1][1] = _squares[0][1];
		_squares[0][1] = tmp;
	}
}

bool CubeFace::isPerfect() const
{
	bool isPerfect = _squares[0][0] == _squares[0][1];
	isPerfect &= _squares[0][1] == _squares[1][0];
	isPerfect &= _squares[1][0] == _squares[1][1];
	return isPerfect;
}
