#include "RubiksCube.hpp"

RubiksCube::TurnIterator::TurnIterator(RubiksCube &rc, bool endIterator) : _ref(rc)
{
	if (endIterator)
	{
		_currentTurn = END;
	}
}

bool RubiksCube::TurnIterator::operator==(TurnIterator other)
{
	return _currentTurn == other._currentTurn;
}

bool RubiksCube::TurnIterator::operator!=(TurnIterator other)
{
	return !(*this == other);
}

void RubiksCube::TurnIterator::operator++(int dummy)
{
	++_currentTurn;
}

RubiksCube RubiksCube::TurnIterator::operator*()
{
	RubiksCube out,
		rc1,
		rc2,
		rc3;

	switch (_currentTurn)
	{
	case TOP_QUARTER_TURN_LEFT:
		out = _ref.QuarterTurn(RubiksCube::TOP, RubiksCube::LEFT);
		break;
	case TOP_QUARTER_TURN_RIGHT:
		out = _ref.QuarterTurn(RubiksCube::TOP, RubiksCube::RIGHT);
		break;
	case BOTTOM_QUARTER_TURN_LEFT:
		out = _ref.QuarterTurn(RubiksCube::BOTTOM, RubiksCube::LEFT);
		break;
	case BOTTOM_QUARTER_TURN_RIGHT:
		out = _ref.QuarterTurn(RubiksCube::BOTTOM, RubiksCube::RIGHT);
		break;
	case LEFT_QUARTER_TURN_UP:
		out = _ref.RotateZ(RubiksCube::RIGHT);
		out = out.QuarterTurn(RubiksCube::TOP, RubiksCube::RIGHT);
		out = out.RotateZ(RubiksCube::LEFT);
		break;
	case LEFT_QUARTER_TURN_DOWN:
		out = _ref.RotateZ(RubiksCube::RIGHT);
		out = out.QuarterTurn(RubiksCube::TOP, RubiksCube::LEFT);
		out = out.RotateZ(RubiksCube::LEFT);
		break;
	case RIGHT_QUARTER_TURN_UP:
		out = _ref.RotateZ(RubiksCube::LEFT);
		out = out.QuarterTurn(RubiksCube::TOP, RubiksCube::LEFT);
		out = out.RotateZ(RubiksCube::RIGHT);
		break;
	case RIGHT_QUARTER_TURN_DOWN:
		out = _ref.RotateZ(RubiksCube::LEFT);
		out = out.QuarterTurn(RubiksCube::TOP, RubiksCube::RIGHT);
		out = out.RotateZ(RubiksCube::RIGHT);
		break;
	case ROTATE_FRONT_LEFT:
		out = _ref.RotateX(RubiksCube::UP);
		out = out.QuarterTurn(RubiksCube::TOP, RubiksCube::RIGHT);
		out = out.RotateX(RubiksCube::DOWN);
		break;
	case ROTATE_FRONT_RIGHT:
		out = _ref.RotateX(RubiksCube::UP);
		out = out.QuarterTurn(RubiksCube::TOP, RubiksCube::LEFT);
		out = out.RotateX(RubiksCube::DOWN);
		break;
	case ROTATE_BACK_LEFT:
		out = _ref.RotateX(RubiksCube::DOWN);
		out = out.QuarterTurn(RubiksCube::TOP, RubiksCube::LEFT);
		out = out.RotateX(RubiksCube::UP);
		break;
	case ROTATE_BACK_RIGHT:
		out = _ref.RotateX(RubiksCube::DOWN);
		out = out.QuarterTurn(RubiksCube::TOP, RubiksCube::RIGHT);
		out = out.RotateX(RubiksCube::UP);
		break;
	}

	return out;
}
