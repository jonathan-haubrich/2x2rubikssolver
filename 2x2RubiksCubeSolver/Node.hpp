#pragma once

#include "RubiksCube.hpp"

#include <limits>

#undef max

template<typename T>
class Node {
public:
	enum {
		WHITE = 0,
		GREY = 1,
		BLACK = 2
	};

	Node(T member);
	Node(T member, int color, int distance);
	Node(const Node<T>& rhs);
	int GetColor();
	void SetColor(int color);
	int GetDistance();
	void SetDistance(int distance);
	Node<T>* GetParent() const;
	void SetParent(Node* parent);
	const T& GetMember() const;

	bool operator==(const Node<T>& other);
	bool operator==(const Node<T>& other) const;
	bool operator==(const Node<T> other);

	friend std::ostream& operator<<(std::ostream& os, Node<T> node);

private:
	int _color{ WHITE };
	int _distance{ std::numeric_limits<int>::max() };
	Node* _parent{ nullptr };
	T _member;
};

template<typename T>
Node<T>::Node(T member) : _member(member) {}

template<typename T>
Node<T>::Node(T member, int color, int distance) : _member(member), _color(color), _distance(distance) {}

template<typename T>
Node<T>::Node(const Node<T>& rhs)
{
	_color = rhs._color;
	_distance = rhs._distance;
	_parent = rhs._parent;
	_member = rhs._member;
}

template<typename T>
int Node<T>::GetColor()
{
	return _color;
}

template<typename T>
void Node<T>::SetColor(int color)
{
	_color = color;
}

template<typename T>
int Node<T>::GetDistance()
{
	return _distance;
}

template<typename T>
void Node<T>::SetDistance(int distance)
{
	_distance = distance;
}

template<typename T>
Node<T>* Node<T>::GetParent() const
{
	return _parent;
}

template<typename T>
void Node<T>::SetParent(Node* parent)
{
	_parent = parent;
}

template<typename T>
const T& Node<T>::GetMember() const
{
	return _member;
}

template<typename T>
bool Node<T>::operator==(const Node<T>& other)
{
	return  _member == other._member;
}

template<typename T>
bool Node<T>::operator==(const Node<T>& other) const
{
	return  _member == other._member;
}

template<typename T>
bool Node<T>::operator==(const Node<T> other)
{
	return  _member == other._member;
}

std::ostream& operator<<(std::ostream& os, Node<RubiksCube> node)
{

	os << "Node:" << std::endl;
	os << "\tColor: " << node.GetColor() << std::endl;
	os << "\tDistance: " << node.GetDistance() << std::endl;
	os << "\tParent: " << node.GetParent() << std::endl;
	os << "Cube State: " << std::endl << node.GetMember();

	return os;
}
