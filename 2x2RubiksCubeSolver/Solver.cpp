﻿#include <Windows.h>

#include <iostream>
#include <queue>
#include <unordered_map>
#include <unordered_set>

#include "Node.hpp"
#include "NodeHash.hpp"
#include "RubiksCube.hpp"
#include "RubiksCubeHash.hpp"

INT main(INT argc, PSTR argv[])
{
	if (argc < 2)
	{
		std::cerr << "Usage: 2x2RubiksCubeSolver <cube_state.txt>" << std::endl;
		return EXIT_FAILURE;
	}

	//Node<RubiksCube> nrc = Node<RubiksCube>(start);
	//std::cout << nrc.GetMember() << std::endl;
	//std::unordered_map<Node<RubiksCube>, std::unordered_set<Node<RubiksCube>>> umnrc;
	//std::hash<Node<RubiksCube>> nrc_hasher = std::hash<Node<RubiksCube>>();
	//std::hash<RubiksCube> rc_hasher = std::hash<RubiksCube>();
	//std::cout << nrc_hasher(nrc) << std::endl;
	//std::cout << rc_hasher(start) << std::endl;

	//std::cout << end << std::endl;

	//int turn = 0;
	//std::string turn_label;

	//for (RubiksCube::TurnIterator ti = end.begin();
	//	ti != end.end();
	//	ti++)
	//{
	//	switch(turn++)
	//	{
	//		case 0:
	//			turn_label = "TOP_QUARTER_TURN_LEFT:";
	//			break;
	//		case 1:
	//			turn_label = "TOP_QUARTER_TURN_RIGHT:";
	//			break;
	//		case 2:
	//			turn_label = "BOTTOM_QUARTER_TURN_LEFT:";
	//			break;
	//		case 3:
	//			turn_label = "BOTTOM_QUARTER_TURN_RIGHT:";
	//			break;
	//		case 4:
	//			turn_label = "LEFT_QUARTER_TURN_UP:";
	//			break;
	//		case 5:
	//			turn_label = "LEFT_QUARTER_TURN_DOWN:";
	//			break;
	//		case 6:
	//			turn_label = "RIGHT_QUARTER_TURN_UP:";
	//			break;
	//		case 7:
	//			turn_label = "RIGHT_QUARTER_TURN_DOWN:";
	//			break;
	//		case 8:
	//			turn_label = "ROTATE_FRONT_LEFT:";
	//			break;
	//		case 9:
	//			turn_label = "ROTATE_FRONT_RIGHT:";
	//			break;
	//		case 10:
	//			turn_label = "ROTATE_BACK_LEFT:";
	//			break;
	//		case 11:
	//			turn_label = "ROTATE_BACK_RIGHT:";
	//			break;
	//	}
	//	current = *ti;
	//	std::cout << turn_label << std::endl;
	//	std::cout << current << std::endl;
	//}
	

	Node<RubiksCube> start = Node<RubiksCube>(RubiksCube(argv[1]),
		Node<RubiksCube>::GREY,
		0);

	std::deque<Node<RubiksCube>*> next_nodes;

	next_nodes.push_back(&start);

	std::unordered_map<Node<RubiksCube>, std::unordered_set<Node<RubiksCube>*>*> um;

	um[start] = new std::unordered_set<Node<RubiksCube>*>;

	int spinner_count = 0,
		last = 0;
	std::string spinners = "|/-\\";

	bool found_perfect = false;

	Node<RubiksCube>* next_node = nullptr,
		* current = nullptr,
		* end_ptr,
		* current_node;

	std::cout << start << std::endl;
	std::cout << "Searching for solution... (this may take awhile)" << std::endl;

	while (!next_nodes.empty() && false == found_perfect)
	{
		next_node = next_nodes.front();
		next_nodes.pop_front();

		for (RubiksCube::TurnIterator it = next_node->GetMember().begin();
			it != next_node->GetMember().end();
			it++)
		{
			current = new Node<RubiksCube>(*it);
			if (um.count(*current) == 0)
			{
				um[*current] = new std::unordered_set<Node<RubiksCube>*>;
				next_nodes.push_back(current);
			}
			if (um.count(*next_node) == 0)
			{
				throw invalid_argument("next_node not found in um??");
			}
			um[*next_node]->insert(current);
			found_perfect = current->GetMember().isPerfect();

			
			if ((um.size() % 100000) == 0)
			{
				std::cout << "\r" << spinners.at(spinner_count % spinners.length()) << "(" << next_nodes.size() << "/" << um.size() << ")";
				++spinner_count;
			}
		}
	}
	std::cout << std::endl;

	if (false == found_perfect)
	{
		std::cout << "[-] Solution NOT FOUND" << std::endl;
		return EXIT_FAILURE;
	}
	else
	{
		std::cout << "[+] Solution FOUND" << std::endl;
	}

	std::cout << "[+] Searching for solution path..." << std::endl;

	end_ptr = current;
	next_nodes.clear();

	next_nodes.push_back(&start);
	current = nullptr;

	while (!next_nodes.empty() && (current != end_ptr))
	{
		current = next_nodes.front();
		next_nodes.pop_front();

		current->SetColor(Node<RubiksCube>::BLACK);

		if (um.count(*current) == 0)
		{
			std::cerr << "current_ptr not found in um!" << std::endl;
			std::cerr << current << std::endl;
			std::cerr << &start << std::endl;
			return EXIT_FAILURE;
		}

		for (std::unordered_set<Node<RubiksCube>*>::iterator it = um[*current]->begin();
			it != um[*current]->end();
			it++)
		{
			current_node = *it;
			if (Node<RubiksCube>::WHITE == current_node->GetColor())
			{
				next_nodes.push_back(current_node);
				current_node->SetColor(Node<RubiksCube>::GREY);
				current_node->SetDistance(current->GetDistance() + 1);
				current_node->SetParent(current);
			}
		}
		if ((next_nodes.size() % 100000) == 0)
		{
			std::cout << "\r" << spinners.at(spinner_count % spinners.length()) << "(" << next_nodes.size() << ")";
		}
	}
	std::cout << "BFS finished! " << next_nodes.size() << std::endl;

	current = end_ptr;
	while (nullptr != current)
	{
		std::cout << *current << std::endl;
		current = current->GetParent();
	}

	return EXIT_SUCCESS;
}