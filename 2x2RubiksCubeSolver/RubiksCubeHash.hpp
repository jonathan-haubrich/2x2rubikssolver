#pragma once

#include <functional>

#include "RubiksCube.hpp"

namespace std
{
	template<>
	struct hash<RubiksCube>
	{
	public:
		size_t operator()(const RubiksCube& rc) const noexcept
		{
			size_t seed = 0;
			std::hash<int> int_hash;
			int row_as_int = 0;

			seed ^= int_hash(rc.GetTop().AsInt()) + 0x9e3779b9 + (seed << 6) + (seed >> 2);

			seed ^= int_hash(rc.GetBottom().AsInt()) + 0x9e3779b9 + (seed << 6) + (seed >> 2);

			for (int i = 0; i < RubiksCube::FACES; ++i)
			{
				seed ^= int_hash(rc.GetFace(i).AsInt()) + 0x9e3779b9 + (seed << 6) + (seed >> 2);
			}

			return seed;
		}
	};
}