#pragma once

#include <Windows.h>

#include <fstream>
#include <iostream>
#include <limits>
#include <string>
#include <sstream>
#include <vector>

#include "CubeFace.hpp"

class RubiksCube {
public:
	enum {
		TOP = 0,
		BOTTOM = 1,
		LEFT = 0,
		RIGHT = 1,
		UP = 2,
		DOWN = 3,
		FACES = 4
	};

	RubiksCube() {}
	RubiksCube(const RubiksCube& rhs);
	RubiksCube(const std::string& filename);

	const CubeFace& GetTop() const;
	const CubeFace& GetBottom() const;
	const CubeFace& GetFace(int face) const;
	bool isPerfect() const;

	RubiksCube QuarterTurn(int row, int direction);
	RubiksCube RotateX(int direction);
	RubiksCube RotateY(int direction);
	RubiksCube RotateZ(int direction);
	
	bool operator==(const RubiksCube& other) const;
	RubiksCube& operator=(const RubiksCube& rhs);

	friend std::ostream& operator<<(std::ostream& os, RubiksCube &rc);
	friend std::ostream& operator<<(std::ostream& os, const RubiksCube& rc);

	class TurnIterator
	{
	public:
		TurnIterator(RubiksCube& rc, bool endIterator = false);
		bool operator==(TurnIterator other);
		bool operator!=(TurnIterator other);
		void operator++(int dummy);
		RubiksCube operator*();

	private:
		enum {
			TOP_QUARTER_TURN_LEFT,
			TOP_QUARTER_TURN_RIGHT,
			BOTTOM_QUARTER_TURN_LEFT,
			BOTTOM_QUARTER_TURN_RIGHT,
			LEFT_QUARTER_TURN_UP,
			LEFT_QUARTER_TURN_DOWN,
			RIGHT_QUARTER_TURN_UP,
			RIGHT_QUARTER_TURN_DOWN,
			ROTATE_FRONT_LEFT,
			ROTATE_FRONT_RIGHT,
			ROTATE_BACK_LEFT,
			ROTATE_BACK_RIGHT,
			END
		};

		RubiksCube &_ref;
		int _currentTurn{ TOP_QUARTER_TURN_LEFT };
	};
	TurnIterator begin() const;
	TurnIterator end() const;

private:
	CubeFace _top{ 'W' };
	CubeFace _bottom{ 'Y' };
	CubeFace _faces[FACES]{ 'G', 'R', 'B', 'O' };

	void ShiftRowsLeft(char *rows[FACES]);
};