#pragma once

#include <iostream>

using std::invalid_argument;

class CubeFace {
public:
	enum {
		ROWS = 2,
		COLUMNS = 2,
		LEFT = 0,
		RIGHT = 1
	};

	CubeFace();
	CubeFace(char id);
	CubeFace(const std::string& face);

	char* GetRow(int row) const;
	const char* GetSquares() const;
	char GetPosition(int i, int j) const;
	int AsInt() const;

	void Rotate(int direction);

	bool isPerfect() const;

	bool operator==(const CubeFace& other) const;
	CubeFace& operator=(const CubeFace& rhs);

	friend std::ostream& operator<<(std::ostream& os, const CubeFace& cf);

private:
	char _squares[ROWS][COLUMNS];
};