﻿#include "RubiksCube.hpp"

std::ostream& operator<<(std::ostream& os, RubiksCube &rc)
{
	PCSTR UPPER_BAR = "    ┌───┐",
		LEFT_UPPER_CORNER = "┌",
		RIGHT_UPPER_CORNER = "┐",
		WALL = "│";

	os << "    ┌───┐" << std::endl;

	for (int i = 0; i < CubeFace::ROWS; ++i)
	{
		os << "    │";
		for (int j = 0; j < CubeFace::COLUMNS; ++j)
		{
			os << rc._top.GetPosition(i, j);
			if (j != CubeFace::COLUMNS - 1)
			{
				os << " ";
			}
		}
		os << "│" << std::endl;
	}

	os << "┌───┼───┼───┬───┐" << std::endl;

	for (int i = 0; i < CubeFace::ROWS; ++i)
	{
		os << "│";
		for (int face = 0; face < RubiksCube::FACES; ++face)
		{
			for (int j = 0; j < CubeFace::COLUMNS; ++j)
			{
				os << rc._faces[face].GetPosition(i, j);
				if (j != CubeFace::COLUMNS - 1)
				{
					os << " ";
				}
			}
			os << "│";;
		}
		os << std::endl;
	}

	os << "└───┼───┼───┴───┘" << std::endl;

	// Print bottom upside down
	for (int i = 0; i < CubeFace::ROWS; ++i)
	{
		os << "    │";
		for (int j = 0; j < CubeFace::COLUMNS; ++j)
		{
			os << rc._bottom.GetPosition(i, j);
			if (j != CubeFace::ROWS - 1)
			{
				os << " ";
			}
		}
		os << "│" << std::endl;
	}
	os << "    └───┘" << std::endl;
	return os;
}

std::ostream& operator<<(std::ostream& os, const RubiksCube& rc)
{
	PCSTR UPPER_BAR = "    ┌───┐",
		LEFT_UPPER_CORNER = "┌",
		RIGHT_UPPER_CORNER = "┐",
		WALL = "│";

	os << "    ┌───┐" << std::endl;

	for (int i = 0; i < CubeFace::ROWS; ++i)
	{
		os << "    │";
		for (int j = 0; j < CubeFace::COLUMNS; ++j)
		{
			os << rc._top.GetPosition(i, j);
			if (j != CubeFace::COLUMNS - 1)
			{
				os << " ";
			}
		}
		os << "│" << std::endl;
	}

	os << "┌───┼───┼───┬───┐" << std::endl;

	for (int i = 0; i < CubeFace::ROWS; ++i)
	{
		os << "│";
		for (int face = 0; face < RubiksCube::FACES; ++face)
		{
			for (int j = 0; j < CubeFace::COLUMNS; ++j)
			{
				os << rc._faces[face].GetPosition(i, j);
				if (j != CubeFace::COLUMNS - 1)
				{
					os << " ";
				}
			}
			os << "│";;
		}
		os << std::endl;
	}

	os << "└───┼───┼───┴───┘" << std::endl;

	// Print bottom upside down
	for (int i = 0; i < CubeFace::ROWS; ++i)
	{
		os << "    │";
		for (int j = 0; j < CubeFace::COLUMNS; ++j)
		{
			os << rc._bottom.GetPosition(i, j);
			if (j != CubeFace::ROWS - 1)
			{
				os << " ";
			}
		}
		os << "│" << std::endl;
	}
	os << "    └───┘" << std::endl;
	return os;
}

bool RubiksCube::operator==(const RubiksCube& other) const
{
	bool isEqual = true;
	isEqual &= (_top == other._top);
	isEqual &= (_bottom == other._bottom);
	for (int i = 0; i < FACES; ++i)
	{
		isEqual &= (_faces[i] == other._faces[i]);
	}
	return isEqual;
}

RubiksCube& RubiksCube::operator=(const RubiksCube& rhs)
{
	_top = rhs._top;
	_bottom = rhs._bottom;
	for (int i = 0; i < FACES; ++i)
	{
		_faces[i] = rhs._faces[i];
	}
	return *this;
}

RubiksCube::TurnIterator RubiksCube::begin() const
{
	return TurnIterator((RubiksCube&)*this);
}

RubiksCube::TurnIterator RubiksCube::end() const
{
	return TurnIterator((RubiksCube&)*this, true);
}

RubiksCube::RubiksCube(const RubiksCube& rhs)
{
	_top = rhs._top;
	_bottom = rhs._bottom;
	for (int i = 0; i < FACES; ++i)
	{
		_faces[i] = rhs._faces[i];
	}
}

RubiksCube::RubiksCube(const std::string& filename)
{
	std::string face;
	std::ifstream file;
	int current_face = 0;
	
	file.open(filename);

	if (!file.is_open())
	{
		throw invalid_argument("Failed to open file: " + filename);
	}

	while(std::getline(file, face))
	{
		// TODO: Change magic value here
		if (face.length() != 4)
		{
			std::stringstream ss;
			ss << "Input file formatted incorrectly" << std::endl;
			ss << "Line: " << current_face << std::endl;
			ss << "Given: " << face << " Length: " << face.length() << " Expected: 4";
			throw invalid_argument(ss.str());
		}

		switch (current_face)
		{
		case 0:
			_top = CubeFace(face);
			break;
		case 1:
		case 2:
		case 3:
		case 4:
			_faces[current_face - 1] = CubeFace(face);
			break;
		case 5:
			_bottom = CubeFace(face);
			break;
		default:
			throw invalid_argument("Faces greather than 6");
			break;
		}

		++current_face;
	}
}

const CubeFace& RubiksCube::GetTop() const
{
	return _top;
}

const CubeFace& RubiksCube::GetBottom() const
{
	return _bottom;
}

const CubeFace& RubiksCube::GetFace(int face) const
{
	if (face >= FACES)
	{
		throw invalid_argument("RubiksCube::GetFace face out of range");
	}

	return _faces[face];
}

bool RubiksCube::isPerfect() const
{
	bool isPerfect = _top.isPerfect();
	isPerfect &= _bottom.isPerfect();
	for (int i = 0; i < FACES; ++i)
	{
		isPerfect &= _faces[i].isPerfect();
	}
	return isPerfect;
}

RubiksCube RubiksCube::QuarterTurn(int row, int direction)
{
	RubiksCube out(*this);

	char *rows[FACES];

	std::vector<int> left_indices = { 0, 1, 2, 3 },
		right_indices = { 3, 2, 1, 0 },
		* indices = &left_indices;

	int face = 0;

	if (RIGHT == direction)
	{
		indices = &right_indices;
	}

	for (std::vector<int>::iterator it = indices->begin();
		it != indices->end();
		it++)
	{
		rows[*it] = out._faces[face].GetRow(row);
		face++;
	}

	ShiftRowsLeft(rows);

	if (TOP == row)
	{
		out._top.Rotate(!direction);
	}
	else
	{
		out._bottom.Rotate(direction);
	}

	return out;
}

RubiksCube RubiksCube::RotateX(int direction)
{
	RubiksCube out(*this);
	CubeFace tmp = out._faces[1];
	switch (direction)
	{
	case UP:
		out._faces[1] = out._bottom;
		
		out._bottom = out._faces[3];
		out._bottom.Rotate(CubeFace::LEFT);
		out._bottom.Rotate(CubeFace::LEFT);

		out._faces[3] = out._top;
		out._faces[3].Rotate(CubeFace::LEFT);
		out._faces[3].Rotate(CubeFace::LEFT);

		out._top = tmp;
		out._faces[0].Rotate(CubeFace::LEFT);
		out._faces[2].Rotate(CubeFace::RIGHT);
		break;
	case DOWN:
		out._faces[1] = out._top;
		out._top = out._faces[3];
		out._top.Rotate(CubeFace::LEFT);
		out._top.Rotate(CubeFace::LEFT);

		out._faces[3] = out._bottom;
		out._faces[3].Rotate(CubeFace::LEFT);
		out._faces[3].Rotate(CubeFace::LEFT);

		out._bottom = tmp;
		out._faces[0].Rotate(CubeFace::RIGHT);
		out._faces[2].Rotate(CubeFace::LEFT);
		break;
	}
	return out;
}

RubiksCube RubiksCube::RotateY(int direction)
{
	RubiksCube out(*this);

	std::vector<int> left_indices = { 0, 1, 2, 3 },
		right_indices = { 3, 2, 1, 0 },
		* indices = nullptr;

	int face = 1,
		step = 1;

	CubeFace tmp;

	if (RIGHT == direction)
	{
		indices = &right_indices;
		face = 2;
		step = 3;	// Use mod to get correct index: 2, 1, 0, 3
	}
	else
	{
		indices = &left_indices;
	}

	tmp = out._faces[indices->front()];

	for (std::vector<int>::iterator it = indices->begin();
		it != indices->end();
		it++)
	{
		out._faces[*it] = out._faces[face % FACES];
		face += step;
	}

	out._faces[indices->back()] = tmp;

	out._top.Rotate(!direction);
	out._bottom.Rotate(direction);

	return out;
}

RubiksCube RubiksCube::RotateZ(int direction)
{
	RubiksCube out(*this);

	CubeFace tmp = out._top;

	if (LEFT == direction)
	{
		out._top = out._faces[2];
		out._top.Rotate(CubeFace::LEFT);
		out._faces[2] = out._bottom;
		out._faces[2].Rotate(CubeFace::LEFT);

		out._bottom = out._faces[0];
		out._bottom.Rotate(CubeFace::LEFT);

		out._faces[0] = tmp;
		out._faces[0].Rotate(CubeFace::LEFT);

		out._faces[1].Rotate(RubiksCube::LEFT);
		out._faces[3].Rotate(RubiksCube::RIGHT);
	} 
	else
	{ 
		out._top = out._faces[0];
		out._top.Rotate(CubeFace::RIGHT);
		out._faces[0] = out._bottom;
		out._faces[0].Rotate(CubeFace::RIGHT);
		out._bottom = out._faces[2];
		out._bottom.Rotate(CubeFace::RIGHT);
		out._faces[2] = tmp;
		out._faces[2].Rotate(CubeFace::RIGHT);

		out._faces[1].Rotate(RubiksCube::RIGHT);
		out._faces[3].Rotate(RubiksCube::LEFT);
	}
	
	return out;
}

void RubiksCube::ShiftRowsLeft(char *rows[FACES])
{
	char tmp[CubeFace::COLUMNS] = { rows[0][0], rows[0][1] };

	for (int i = 0; i < FACES - 1; ++i)
	{
		rows[i][0] = rows[i + 1][0];
		rows[i][1] = rows[i + 1][1];
	}

	rows[FACES - 1][0] = tmp[0];
	rows[FACES - 1][1] = tmp[1];
}
