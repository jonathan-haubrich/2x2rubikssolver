#pragma once

#include <functional>
#include <iostream>

#include "Node.hpp"
#include "RubiksCube.hpp"
#include "RubiksCubeHash.hpp"

namespace std
{
	template<>
	struct hash<Node<RubiksCube>>
	{
	public:
		size_t operator()(const Node<RubiksCube>& n) const noexcept
		{
			hash<RubiksCube> rc_hasher = hash<RubiksCube>();
			return rc_hasher(n.GetMember());
		}

		size_t operator()(Node<RubiksCube>& n) const noexcept
		{
			hash<RubiksCube> rc_hasher = hash<RubiksCube>();
			return rc_hasher(n.GetMember());
		}
	};
}

namespace std
{
	template<>
	struct hash<Node<RubiksCube>*>
	{
	public:
		size_t operator()(Node<RubiksCube>* n) const noexcept
		{
			hash<RubiksCube> rc_hasher = hash<RubiksCube>();
			return rc_hasher(n->GetMember());
		}
	};
}